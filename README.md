# Apartment Reservation Manager

## Description
The goal of this project is to practiaclly try out the technologies and tools learned at Databases 2 course at University of Belgrade, School of Electrical Engineering. These technologies are: xml, xQuery, xPath, BaseX, MySQL Workbench, JPA and Netbeans.

## Usage

The database itself is provided by class lecturer and is located in `database/xml/` directory of this repo. It is in xml format.

First step is to create `sql` database from the one provided by the course staff. This can be done by executing the `kreiraj_bazu.sql` script located in `database/sql/` directory. Next step is to populate it with data from `xml` database. This is done using xQuery, xPath and BaseX tool. For this all `.xql` scripts from directory `database/xml/` are used. They generate appropriate `sql` scripts which fill the `sql` database.

All source files are located inside `/jpa/Baze/` directory. There are two subfolders:
 - `baze` which contains:
   - `Baze.java` - executes sql scripts that fill the database,
   - `Scenario1.java` - first usecase,
   - `Scenario2.java` - second usecase.
 - `entiteti` which contains all entities mapped from database.
 