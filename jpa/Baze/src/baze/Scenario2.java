package baze;

import entiteti.Apartman;
import entiteti.Korisnik;
import entiteti.Rezervacija;
import entiteti.RezervacijaPK;
import entiteti.Soba;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author ivankulezic
 */
public class Scenario2 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BazePU");
        EntityManager em = emf.createEntityManager();

        Scanner input = new Scanner(System.in);
        System.out.println("\n Dobrodosli u aplikaciju za pretragu aranzmana. Da biste zapoceli pretragu prvo se ulogujte.\n");
        System.out.print("    korisnicko ime: ");
        String kor_ime = input.next();
        System.out.print("    lozinka: ");
        String lozinka = input.next();

        Query nadji_korisnika = em.createNativeQuery("SELECT * FROM Korisnik where kor_ime = '" + kor_ime
                + "' AND sifra = '" + lozinka + "';", Korisnik.class);

        try {
            Korisnik korisnik = (Korisnik) nadji_korisnika.getSingleResult();
            System.out.println("\n Dobrodosli, " + korisnik.getIme() + " " + korisnik.getPrezime());
        } catch (NoResultException e) {
            System.out.println("\n\nNekorektni login podaci!\n");
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nUnesite parametre pretrage apartmana:\n");
        System.out.print(" drzava: ");
        String drzava = input.next();
        System.out.print("\n grad(ukoliko ne zelite pretragu po gradu unesite x): ");
        String grad = input.next();

        String pretraga_apartmana = "SELECT * FROM Apartman where LOWER(drzava) LIKE '"
                + drzava.toLowerCase() + "'";
        if (!grad.equals("x")) {
            pretraga_apartmana = pretraga_apartmana.concat(" AND LOWER(grad) LIKE '"
                    + grad.toLowerCase() + "'");
        }
        pretraga_apartmana = pretraga_apartmana.concat(";");

        Query nadji_apartmane = em.createNativeQuery(pretraga_apartmana, Apartman.class);

        List<Apartman> apartmani = nadji_apartmane.getResultList();

        System.out.println("\nrezultati pretrage:\n");

        if (apartmani.isEmpty()) {
            System.out.println("Nema rezultata pretrage za zadati kriterijum");
        }

        System.out.println("R.B.     |        Naziv                            |      Adresa");
        System.out.println("----------------------------------------------------------------------------");
        int i = 1;
        for (Apartman a : apartmani) {
            System.out.println(i + "       " + a.getNaziv() + "                        "
                    + a.getUlica() + " " + a.getBroj());
            i++;
        }

        System.out.print("\n\nIzaberite apartman(unesite R.B.):");
        int id = input.nextInt();
        System.out.print("\nPocetak boravka(yyyy-mm-dd): ");
        String pocetak = input.next();
        System.out.print("\nKraj boravka(yyyy-mm-dd): ");
        String kraj = input.next();

        String nadji_sobe = "SELECT s.* FROM Soba s, Rezervacija r WHERE s.ID_S = r.ID_s AND s.ID_A = " + id
                + " AND (r.pocetak >= '" + pocetak + "' AND r.pocetak >= '" + kraj + "' OR r.kraj <= '" + pocetak
                + "' AND r.kraj <='" + kraj + "')\n"
                + "UNION SELECT * FROM Soba soba WHERE soba.ID_S NOT IN (SELECT ID_S FROM Rezervacija) AND soba.ID_A = " + id + ";";

        System.out.println(nadji_sobe);
        
        List<Soba> sobe = em.createNativeQuery(nadji_sobe, Soba.class).getResultList();

        if (sobe.isEmpty()) {
            System.out.println("\nNema slobodnih soba za zadati period.");
            return;
        }

        System.out.println("\nSlobodne sobe:");

        System.out.println("\n    ID    |     broj_osoba   |   redni broj   |   opis      ");
        System.out.println("---------------------------------------------------------------");

        for (Soba s : sobe) {
            System.out.println("    " + s.getIdS() + "                  " + s.getBrojOsoba() + "      " + 
                    s.getRedniBroj() + "      " + s.getOpis());
        }

        System.out.print("\nUnesite ID sobe: ");
        int id_sobe = input.nextInt();
        
        Rezervacija rez = new Rezervacija();
        rez.setRezervacijaPK(new RezervacijaPK(kor_ime, id_sobe));
        rez.setPocetak(new Date(
                Integer.parseInt(pocetak.substring(0, 4)) - 1900,
                Integer.parseInt(pocetak.substring(5, 7)) - 1,
                Integer.parseInt(pocetak.substring(8))));
        rez.setKraj(new Date(
                Integer.parseInt(kraj.substring(0, 4)) - 1900,
                Integer.parseInt(kraj.substring(5, 7)) - 1,
                Integer.parseInt(kraj.substring(8))));
        
        em.getTransaction().begin();
        em.persist(rez);
        em.getTransaction().commit();
        
        System.out.println("\nRezervacija uspesna!");
        
        em.close();
        emf.close();
    }
}
