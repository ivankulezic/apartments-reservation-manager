package baze;

import entiteti.Rezervacija;
import entiteti.RezervacijaPK;
import entiteti.Soba;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author ivankulezic
 */
public class Baze {

    public static void popuniBazu(EntityManager manager) {
        for (File skripta : new File("skripte").listFiles()) {
            try (Stream<String> stream = Files.lines(Paths.get("skripte/" + skripta.getName()))) {
                stream.forEach(line -> {
                    manager.getTransaction().begin();
                    manager.createNativeQuery(line).executeUpdate();
                    manager.getTransaction().commit();
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void popuniRezervacije(EntityManager manager) {
        manager.getTransaction().begin();
        Query query = manager.createNativeQuery("SELECT * FROM Soba", Soba.class);
        List<Soba> sobe = query.getResultList();
        int i = 0;
        String poslednjiKorisnik = null;
        try (Stream<String> stream = Files.lines(Paths.get("rezervacije/rezervacije.txt"))) {
            for (String line : stream.collect(Collectors.toList())) {
                RezervacijaPK rpk = new RezervacijaPK(line, sobe.get(i).getIdS());
                Rezervacija r = new Rezervacija();
                r.setRezervacijaPK(rpk);
                r.setPocetak(new Date(117, i % 12, (i + 1) % 29));
                r.setKraj(new Date(117, i  % 12, (i + 1) % 29 + 5));
                manager.persist(r);
                i++;
                poslednjiKorisnik = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        RezervacijaPK rpk = new RezervacijaPK(poslednjiKorisnik, sobe.get(0).getIdS());
        Rezervacija r = new Rezervacija();
        r.setRezervacijaPK(rpk);
        r.setPocetak(new Date(117, 4, 13));
        r.setKraj(new Date(117, 4, 21));
        manager.persist(r);
        manager.getTransaction().commit();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BazePU");
        EntityManager em = emf.createEntityManager();

        popuniBazu(em);

        popuniRezervacije(em);

        em.close();
        emf.close();
    }

}
