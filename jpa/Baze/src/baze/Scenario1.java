/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baze;

import entiteti.Apartman;
import entiteti.Korisnik;
import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author ivankulezic
 */
public class Scenario1 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("BazePU");
        EntityManager em = emf.createEntityManager();

        Scanner input = new Scanner(System.in);
        System.out.println("\n Dobrodosli u aplikaciju za pretragu aranzmana. Da biste zapoceli pretragu prvo se ulogujte.\n");
        System.out.print("    korisnicko ime: ");
        String kor_ime = input.next();
        System.out.print("    lozinka: ");
        String lozinka = input.next();

        Query nadji_korisnika = em.createNativeQuery("SELECT * FROM Korisnik where kor_ime = '" + kor_ime
                + "' AND sifra = '" + lozinka + "';", Korisnik.class);
        
        try {
            Korisnik korisnik = (Korisnik) nadji_korisnika.getSingleResult();
            System.out.println("\n Dobrodosli, " + korisnik.getIme() + " " + korisnik.getPrezime());
        } catch (NoResultException e) {
            System.out.println("\n\nNekorektni login podaci!\n");
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nUnesite parametre pretrage apartmana:\n");
        System.out.print(" drzava: ");
        String drzava = input.next();
        System.out.print("\n grad(ukoliko ne zelite pretragu po gradu unesite x): ");
        String grad = input.next();
        
        String pretraga_apartmana = "SELECT * FROM Apartman where LOWER(drzava) LIKE '" +
                drzava.toLowerCase() + "'";
        if(!grad.equals("x")){
            pretraga_apartmana = pretraga_apartmana.concat(" AND LOWER(grad) LIKE '" +
                    grad.toLowerCase() + "'");
        }
        pretraga_apartmana = pretraga_apartmana.concat(";");
        
        Query nadji_apartmane = em.createNativeQuery(pretraga_apartmana, Apartman.class);
        
        List<Apartman> apartmani = nadji_apartmane.getResultList();
        
        System.out.println("\nrezultati pretrage:\n");
        
        if(apartmani.isEmpty()){
            System.out.println("Nema rezultata pretrage za zadati kriterijum");
        }
        
        for(Apartman a : apartmani){
            System.out.println("Naziv: " + a.getNaziv() + " Adresa: " + 
                    a.getUlica() + " " + a.getBroj());
        }
        
        em.close();
        emf.close();
    }
}
