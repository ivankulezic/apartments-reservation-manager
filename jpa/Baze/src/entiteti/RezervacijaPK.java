/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author ivankulezic
 */
@Embeddable
public class RezervacijaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "kor_ime")
    private String korIme;
    @Basic(optional = false)
    @Column(name = "ID_S")
    private int idS;

    public RezervacijaPK() {
    }

    public RezervacijaPK(String korIme, int idS) {
        this.korIme = korIme;
        this.idS = idS;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public int getIdS() {
        return idS;
    }

    public void setIdS(int idS) {
        this.idS = idS;
    }
    
}
