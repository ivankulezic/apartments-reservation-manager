package entiteti;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivankulezic
 */
@Entity
@Table(name = "Apartman")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Apartman.findAll", query = "SELECT a FROM Apartman a")
    , @NamedQuery(name = "Apartman.findByIdA", query = "SELECT a FROM Apartman a WHERE a.idA = :idA")
    , @NamedQuery(name = "Apartman.findByBroj", query = "SELECT a FROM Apartman a WHERE a.broj = :broj")
    , @NamedQuery(name = "Apartman.findByDrzava", query = "SELECT a FROM Apartman a WHERE a.drzava = :drzava")
    , @NamedQuery(name = "Apartman.findByGrad", query = "SELECT a FROM Apartman a WHERE a.grad = :grad")
    , @NamedQuery(name = "Apartman.findByKorIme", query = "SELECT a FROM Apartman a WHERE a.korIme = :korIme")
    , @NamedQuery(name = "Apartman.findByNaziv", query = "SELECT a FROM Apartman a WHERE a.naziv = :naziv")
    , @NamedQuery(name = "Apartman.findByOpis", query = "SELECT a FROM Apartman a WHERE a.opis = :opis")
    , @NamedQuery(name = "Apartman.findByUlica", query = "SELECT a FROM Apartman a WHERE a.ulica = :ulica")
    , @NamedQuery(name = "Apartman.findByZakljucan", query = "SELECT a FROM Apartman a WHERE a.zakljucan = :zakljucan")})
public class Apartman implements Serializable {

    @OneToMany(mappedBy = "idA")
    private Collection<Soba> sobaCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_A")
    private Integer idA;
    @Column(name = "broj")
    private String broj;
    @Column(name = "drzava")
    private String drzava;
    @Column(name = "grad")
    private String grad;
    @Column(name = "kor_ime")
    private String korIme;
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "opis")
    private String opis;
    @Column(name = "ulica")
    private String ulica;
    @Column(name = "zakljucan")
    private Short zakljucan;

    public Apartman() {
    }

    public Apartman(Integer idA) {
        this.idA = idA;
    }

    public Integer getIdA() {
        return idA;
    }

    public void setIdA(Integer idA) {
        this.idA = idA;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getKorIme() {
        return korIme;
    }

    public void setKorIme(String korIme) {
        this.korIme = korIme;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public Short getZakljucan() {
        return zakljucan;
    }

    public void setZakljucan(Short zakljucan) {
        this.zakljucan = zakljucan;
    }
    
}
