/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entiteti;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivankulezic
 */
@Entity
@Table(name = "Rezervacija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rezervacija.findAll", query = "SELECT r FROM Rezervacija r")
    , @NamedQuery(name = "Rezervacija.findByKorIme", query = "SELECT r FROM Rezervacija r WHERE r.rezervacijaPK.korIme = :korIme")
    , @NamedQuery(name = "Rezervacija.findByIdS", query = "SELECT r FROM Rezervacija r WHERE r.rezervacijaPK.idS = :idS")
    , @NamedQuery(name = "Rezervacija.findByPocetak", query = "SELECT r FROM Rezervacija r WHERE r.pocetak = :pocetak")
    , @NamedQuery(name = "Rezervacija.findByKraj", query = "SELECT r FROM Rezervacija r WHERE r.kraj = :kraj")})
public class Rezervacija implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RezervacijaPK rezervacijaPK;
    @Column(name = "pocetak")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pocetak;
    @Column(name = "kraj")
    @Temporal(TemporalType.TIMESTAMP)
    private Date kraj;

    public Rezervacija() {
    }

    public Rezervacija(RezervacijaPK rezervacijaPK) {
        this.rezervacijaPK = rezervacijaPK;
    }

    public Rezervacija(String korIme, int idS) {
        this.rezervacijaPK = new RezervacijaPK(korIme, idS);
    }

    public RezervacijaPK getRezervacijaPK() {
        return rezervacijaPK;
    }

    public void setRezervacijaPK(RezervacijaPK rezervacijaPK) {
        this.rezervacijaPK = rezervacijaPK;
    }

    public Date getPocetak() {
        return pocetak;
    }

    public void setPocetak(Date pocetak) {
        this.pocetak = pocetak;
    }

    public Date getKraj() {
        return kraj;
    }

    public void setKraj(Date kraj) {
        this.kraj = kraj;
    }
    
}
