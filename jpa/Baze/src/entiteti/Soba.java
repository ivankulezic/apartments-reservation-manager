/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivankulezic
 */
@Entity
@Table(name = "Soba")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Soba.findAll", query = "SELECT s FROM Soba s")
    , @NamedQuery(name = "Soba.findByIdS", query = "SELECT s FROM Soba s WHERE s.idS = :idS")
    , @NamedQuery(name = "Soba.findByBrojOsoba", query = "SELECT s FROM Soba s WHERE s.brojOsoba = :brojOsoba")
    , @NamedQuery(name = "Soba.findByOpis", query = "SELECT s FROM Soba s WHERE s.opis = :opis")
    , @NamedQuery(name = "Soba.findByRedniBroj", query = "SELECT s FROM Soba s WHERE s.redniBroj = :redniBroj")
    , @NamedQuery(name = "Soba.findByZakljucana", query = "SELECT s FROM Soba s WHERE s.zakljucana = :zakljucana")})
public class Soba implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_S")
    private Integer idS;
    @Column(name = "broj_osoba")
    private Integer brojOsoba;
    @Column(name = "opis")
    private String opis;
    @Column(name = "redni_broj")
    private Integer redniBroj;
    @Column(name = "zakljucana")
    private Short zakljucana;
    @JoinColumn(name = "ID_A", referencedColumnName = "ID_A")
    @ManyToOne
    private Apartman idA;

    public Soba() {
    }

    public Soba(Integer idS) {
        this.idS = idS;
    }

    public Integer getIdS() {
        return idS;
    }

    public void setIdS(Integer idS) {
        this.idS = idS;
    }

    public Integer getBrojOsoba() {
        return brojOsoba;
    }

    public void setBrojOsoba(Integer brojOsoba) {
        this.brojOsoba = brojOsoba;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Integer getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(Integer redniBroj) {
        this.redniBroj = redniBroj;
    }

    public Short getZakljucana() {
        return zakljucana;
    }

    public void setZakljucana(Short zakljucana) {
        this.zakljucana = zakljucana;
    }

    public Apartman getIdA() {
        return idA;
    }

    public void setIdA(Apartman idA) {
        this.idA = idA;
    }
    
}
